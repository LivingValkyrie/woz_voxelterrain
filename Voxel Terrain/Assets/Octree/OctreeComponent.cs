﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: OctreeComponent
/// </summary>
public class OctreeComponent : MonoBehaviour {
	#region Fields

	public float size = 5;
	[Range(0, 4)]
	public int depth = 2;

	#endregion

	void Start() {}

	void Update() {}

	void OnDrawGizmos() {
		var octree = new Octree<int>(transform.position, size, depth);

		DrawNode(octree.GetRoot());
	}

	void DrawNode(Octree<int>.OctreeNode<int> node) {
		if (node.IsLeaf()) {
			Gizmos.color = Color.green;
		} else {
			Gizmos.color = Color.blue;
			foreach (var subNode in node.Nodes) {
				DrawNode(subNode);
			}
		}

		Gizmos.DrawWireCube(node.Position, Vector3.one * node.Size);
	}
}