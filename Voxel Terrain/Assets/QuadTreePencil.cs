﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: QuadTreePencil
/// </summary>
[RequireComponent(typeof (QuadTreeComponent))]
public class QuadTreePencil : MonoBehaviour {
	#region Fields

	public QuadTreeComponent quadTree;

	#endregion

	void Start() {
		quadTree = GetComponent<QuadTreeComponent>();
	}

	void Update() {
		var insertionPoint = Camera.main.ScreenPointToRay(Input.mousePosition);

		if (Input.GetMouseButton(0)) {
			//remove
			//print("Delete: " + insertionPoint.origin );

			quadTree.QuadTree.Insert(insertionPoint.origin, false);
		} else if (Input.GetMouseButton(1)) {
			//insert
			//print( "Insert: " + insertionPoint.origin );

			quadTree.QuadTree.Insert(insertionPoint.origin, true);
		}
	}
}