﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: QuadMeshGenerator
/// </summary>
public class QuadMeshGenerator : MonoBehaviour {
	#region Fields

	public bool generate = false;
	public QuadTreeComponent quadTree;
	public Material voxelMaterial;

	//public Color color;
	GameObject prevMesh;
	bool initialized; 

	#endregion

	void Start() {
		quadTree = GetComponent<QuadTreeComponent>();
	}

	void Update() {
		if (quadTree.QuadTree != null) {
			initialized = true;
			quadTree.QuadTree.QuadTreeUpdated += (obj, args) => { generate = true; };
		}


		if (generate) {
			Stopwatch watch = new Stopwatch();
			watch.Start();
			var generatedMesh = GenerateMesh();
			watch.Stop();
			//print("Milliseconds used to generate mesh: " + watch.ElapsedMilliseconds);

			if (prevMesh != null) {
				Destroy(prevMesh);
			}
			prevMesh = generatedMesh;

			generate = false;
		}
	}

	GameObject GenerateMesh() {
		GameObject chunk = new GameObject("Voxel Mesh");
		chunk.transform.parent = transform;
		chunk.transform.localPosition = Vector3.zero;

		var mesh = new Mesh();
		var vertices = new List<Vector3>();
		var triangles = new List<int>();
		var uvs = new List<Vector2>();
		var normals = new List<Vector3>();

		foreach (var leaf in quadTree.QuadTree.GetLeafNodes().Where((node) => node.Value)) {
			
			Vector3 upperLeft = new Vector3(leaf.Position.x - leaf.Size * 0.5f, leaf.Position.y + leaf.Size * 0.5f, 0);
			var initialIndex = vertices.Count;

			//create square verts
			vertices.Add(upperLeft);
			vertices.Add(upperLeft + Vector3.right * leaf.Size);
			vertices.Add(upperLeft + Vector3.down * leaf.Size);
			vertices.Add(upperLeft + Vector3.right * leaf.Size + Vector3.down * leaf.Size);

			uvs.Add(upperLeft);
			uvs.Add(upperLeft + Vector3.right * leaf.Size);
			uvs.Add(upperLeft + Vector3.down * leaf.Size);
			uvs.Add(upperLeft + Vector3.right * leaf.Size + Vector3.down * leaf.Size);

			normals.Add(Vector3.back);
			normals.Add(Vector3.back);
			normals.Add(Vector3.back);
			normals.Add(Vector3.back);

			//first tri
			triangles.Add(initialIndex);
			triangles.Add(initialIndex + 1);
			triangles.Add(initialIndex + 2);

			//second
			triangles.Add(initialIndex + 3);
			triangles.Add(initialIndex + 2);
			triangles.Add(initialIndex + 1);
		}

		mesh.SetVertices(vertices);
		mesh.SetTriangles(triangles, 0);
		mesh.SetUVs(0, uvs);
		mesh.SetNormals(normals);

		var meshFilter = chunk.AddComponent<MeshFilter>();
		var meshRenderer = chunk.AddComponent<MeshRenderer>();
		meshRenderer.material = voxelMaterial;

		meshFilter.mesh = mesh;

		return chunk;
	}
}