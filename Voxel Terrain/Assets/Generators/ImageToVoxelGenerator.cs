﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: ImageToVoxelGenerator
/// </summary>
public class ImageToVoxelGenerator : MonoBehaviour {
	#region Fields

	public Texture2D image;
	public QuadTreeComponent quadTree;
	public float threshold = 0.05f;
	float oldThreshold;
	#endregion

	void Start() {
		quadTree = GetComponent<QuadTreeComponent>();
		Generate();
		oldThreshold = threshold;
	}

	void Update() {
		if (threshold != oldThreshold) {
			oldThreshold = threshold;
			Generate();
		}
	}

	void Generate() {
		quadTree.QuadTree.Clear();
		foreach (var VARIABLE in transform.GetComponentsInChildren<Transform>()) {
			if (VARIABLE != transform) {
				
				Destroy(VARIABLE.gameObject);	
			}
		}
		

		int cells = (int) Mathf.Pow(2, quadTree.depth);

		for (int x = 0; x <= cells; x++) {
			for (int y = 0; y <= cells; y++) {
				Vector2 position = quadTree.transform.position;
				position.x += (x - cells / 2) / (float) cells * quadTree.size;
				position.y += (y - cells / 2) / (float) cells * quadTree.size;

				var pixel = image.GetPixelBilinear(x / (float) cells, y / (float) cells);

				if (pixel.r >= threshold) {
					quadTree.QuadTree.Insert(position, true);
				}
			}
		}

		GetComponent<QuadMeshGenerator>().generate = true;
	}
	
}