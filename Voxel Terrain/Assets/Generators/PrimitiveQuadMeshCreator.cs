﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PrimitiveQuadMeshCreator
/// </summary>
public class PrimitiveQuadMeshCreator : MonoBehaviour {
	#region Fields

	public bool generate = false;
	public QuadTreeComponent quadTree;

	//public Color color;

	#endregion

	void Start() {
		quadTree = GetComponent<QuadTreeComponent>();
	}

	void Update() {
		if (generate) {
			GenerateMesh();
			generate = false;
		}
	}

	void GenerateMesh() {
		foreach (var leaf in quadTree.QuadTree.GetLeafNodes()) {
			if (leaf.Value) {
				
			var go = GameObject.CreatePrimitive(PrimitiveType.Quad);
				go.transform.parent = quadTree.transform;
				go.transform.position = leaf.Position;
				go.transform.localScale = Vector3.one * leaf.Size;
			
			}

			//go.GetComponent<MeshRenderer>().voxelMaterial.color = color * (leaf.Size / 2);
		}
	}
}