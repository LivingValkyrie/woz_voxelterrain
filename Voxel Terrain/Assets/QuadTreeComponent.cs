﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: QuadTreeComponent
/// </summary>
public class QuadTreeComponent : MonoBehaviour {
	#region Fields

	public float size = 5;

	//[Range(0, 6)]
	public int depth = 2;

	QuadTree<bool> quadTree;

	public QuadTree<bool> QuadTree {
		get { return quadTree; }
	}

	#endregion

	void Awake() {
		quadTree = new QuadTree<bool>(transform.position, size, depth);
	}

	void Update() {}

	void OnDrawGizmos() {
		if (quadTree != null) {
			DrawNode(quadTree.GetRoot());
		}
	}

	Color minColor = new Color(1, 1, 1, 1f);
	Color maxColor = new Color(0, 0.5f, 1, 0.25f);

	void DrawNode(QuadTree<bool>.QuadTreeNode<bool> node, int nodeDepth = 0) {
		if (!node.IsLeaf()) {
			if (node.Nodes != null) {
				foreach (var subNode in node.Nodes) {
					if (subNode != null) {
						DrawNode(subNode, nodeDepth + 1);
					}
				}
			}
		}

		Gizmos.color = Color.Lerp(minColor, maxColor, nodeDepth / (float) depth);

		Gizmos.DrawWireCube(node.Position, new Vector3(1, 1, 0.1f) * node.Size);
	}
}