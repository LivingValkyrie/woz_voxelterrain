﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: QuadTreeCircleInsertion
/// </summary>
[RequireComponent(typeof (QuadTreeComponent))]
public class QuadTreeCircleInsertion : MonoBehaviour {
	#region Fields

	public QuadTreeComponent quadTree;
	public float radius = 0.5f;

	#endregion

	void Start() {
		quadTree = GetComponent<QuadTreeComponent>();
	}

	void Update() {
		var insertionPoint = Camera.main.ScreenPointToRay(Input.mousePosition);

		if (Input.GetMouseButton(0)) {
			//remove
			print("Delete: " + insertionPoint.origin);

			quadTree.QuadTree.InsertCircle(insertionPoint.origin,radius, false);
		} else if (Input.GetMouseButton(1)) {
			//insert
			print("Insert: " + insertionPoint.origin);

			quadTree.QuadTree.InsertCircle(insertionPoint.origin,radius, true);
		}
	}
}