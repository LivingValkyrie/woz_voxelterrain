﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum QuadTreeIndex {

	//00-01
	//10-11

	TopLeft = 0, //00
	TopRight = 1, //01
	BottomLeft = 2, //10
	BottomRight = 3, //01
}

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: QuadTree
/// </summary>
public class QuadTree<TType> where TType :  IComparable {
	QuadTreeNode<TType> node;
	int depth;

	public event EventHandler QuadTreeUpdated;

	void NotifyQuadtreeUpdate() {
		if (QuadTreeUpdated != null) {
			QuadTreeUpdated(this, new EventArgs());
		}
	}

	public QuadTree(Vector2 position, float size, int depth) {
		node = new QuadTreeNode<TType>(position, size, depth);
		this.depth = depth;

		//node.Subdivide(depth);
	}

	static int GetIndexOfPosition(Vector2 lookupPosition, Vector2 nodePosition) {
		int index = 0;

		//if lookup is higher than the node then it is equal to 0 (upper), else it is equal to 4 (bottom)
		//still not sure how this works
		//use < for y because the y is greater when lower
		index |= lookupPosition.y < nodePosition.y ? 2 : 0;
		index |= lookupPosition.x > nodePosition.x ? 1 : 0;

		return index;
	}

	public void Clear() {
		node.Clear();
	}

	public void Insert(Vector2 position, TType value) {
		//maybe name "insertedNode"
		var leafNode = node.Subdivide(position, value, depth);
		leafNode.Value = value;
		NotifyQuadtreeUpdate();
	}

	public void InsertCircle(Vector2 position, float radius, TType value) {
		//maybe name "insertedNode"
		var leafNodes = new LinkedList<QuadTreeNode<TType>>();
		node.CircleSubdivide( leafNodes, position, radius, value, depth);
		NotifyQuadtreeUpdate();
	}

	public QuadTreeNode<TType> GetRoot() {
		return node;
	}

	public IEnumerable<QuadTreeNode<TType>> GetLeafNodes() {
		return node.GetLeafNodes();
	}

	public class QuadTreeNode<TType> where TType : IComparable{
		#region Fields

		Vector2 position;
		float size;
		QuadTreeNode<TType>[] subNodes;
		TType _value;
		int depth;
		public float Size {
			get { return size; }
		}
		public Vector2 Position {
			get { return position; }
		}
		public IEnumerable<QuadTreeNode<TType>> Nodes {
			get { return subNodes; }
		}
		public TType Value {
			get { return _value; }
			internal set { _value = value; }
		}

		#endregion

		public void Clear() {
			subNodes = null;
		}

		public QuadTreeNode(Vector2 position, float size, int depth, TType value = default(TType)) {
			this._value = value;
			this.position = position;
			this.size = size;
			this.depth = depth;
		}

		public QuadTreeNode<TType> Subdivide(Vector2 targetPosition, TType value, int depth = 0) {
			if (depth == 0) {
				return this;
			}

			int subDivIndex = GetIndexOfPosition(targetPosition, position);
			if (subNodes == null) {
				subNodes = new QuadTreeNode<TType>[4];

				for (int i = 0; i < subNodes.Length; i++) {
					Vector2 newPos = position;

					//looks at the bit in that place, 1 is first, 2 is second, 4th is third. if that bit is true then it passes
					if ((i & 2) == 2) {
						newPos.y -= size * 0.25f;
					} else {
						newPos.y += size * 0.25f;
					}

					if ((i & 1) == 1) {
						newPos.x += size * 0.25f;
					} else {
						newPos.x -= size * 0.25f;
					}

					subNodes[i] = new QuadTreeNode<TType>(newPos, size * 0.5f, depth - 1);
					if (depth > 0 && subDivIndex == i) {
						subNodes[i].Subdivide(targetPosition, value, depth - 1);
					}
				}
			}

			return subNodes[subDivIndex].Subdivide(targetPosition, value, depth - 1);
		}

		public void CircleSubdivide(LinkedList<QuadTreeNode<TType>> selectedNodes ,Vector2 targetPosition, float radius, TType value, int depth = 0) {
			if (depth == 0) {
				this.Value = value;
				selectedNodes.AddLast(this);
				return;
			}

			//int subDivIndex = GetIndexOfPosition(targetPosition, position);
			if (subNodes == null) {
				subNodes = new QuadTreeNode<TType>[4];

				for (int i = 0; i < subNodes.Length; i++) {
					Vector2 newPos = position;

					//looks at the bit in that place, 1 is first, 2 is second, 4th is third. if that bit is true then it passes
					if ((i & 2) == 2) {
						newPos.y -= size * 0.25f;
					} else {
						newPos.y += size * 0.25f;
					}

					if ((i & 1) == 1) {
						newPos.x += size * 0.25f;
					} else {
						newPos.x -= size * 0.25f;
					}

					subNodes[i] = new QuadTreeNode<TType>(newPos, size * 0.5f, depth - 1, Value );
				}
			}

			for (int i = 0; i < subNodes.Length; i++) {
				if (subNodes[i].ContainedInCircle(targetPosition, radius)) {
					subNodes[i].CircleSubdivide(selectedNodes,targetPosition, radius, value, depth - 1);
				}
			}

			bool shouldReduce = true;
			var initialValue = subNodes[0].Value;
			for (int i = 0; i < subNodes.Length; i++) {
				//bitwise and. becomes true if both are true
				shouldReduce &= (initialValue.CompareTo(subNodes[i].Value) == 0);
				shouldReduce &= (subNodes[i].IsLeaf());
			}

			if (shouldReduce) {
				this.Value = initialValue;
				subNodes = null;
			}
		}

		public bool ContainedInCircle(Vector2 position, float radius) {
			Vector2 difference = this.position - position;

			difference.x = Mathf.Max(0, Mathf.Abs(difference.x) - size / 2);
			difference.y = Mathf.Max(0, Mathf.Abs(difference.y) - size / 2);
			return difference.magnitude < radius;
		}

		public bool IsLeaf() {
			return Nodes == null;
		}

		public IEnumerable<QuadTreeNode<TType>> GetLeafNodes() {
			if (IsLeaf()) {
				yield return this;
			} else {
				if (subNodes != null) {
					foreach (var subNode in subNodes) {
						foreach (var leaf in subNode.GetLeafNodes()) {
							yield return leaf;
						}
					}
				}
			}
		}
	}

}